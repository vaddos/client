#ifndef CLIENT_H
#define CLIENT_H
#include "AClient.h"
#include <string>
#include <queue>
#include <mutex>
#include <atomic>
#include <thread>

class Client : public AClient
{

    std::queue<std::string> outQueque;
    std::mutex outMutex;
    std::atomic<bool> running;
public:
    std::thread thrClient;
    typedef int connection_desc;
    Client();
    virtual bool openSocket();
    virtual bool isSocketOpen();
    virtual bool connectToServ(unsigned int portNumber,const std::string &ipAddr);
    virtual void sendMes(const std::string &mes);
    virtual bool isRunning();
    virtual void stop();
    ~Client();
    void run();
protected:

    bool recieve();
    bool sendMes();
private:
    connection_desc clientSockDesc;
    Client(const Client&);
    Client & operator = (const Client &s);
};

#endif // CLIENT_H
