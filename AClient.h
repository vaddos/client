#ifndef ACLIENT_H
#define ACLIENT_H
#include <string>

class AClient
{
public:
    virtual bool openSocket() = 0;
    virtual bool isSocketOpen() = 0;
    virtual bool connectToServ(unsigned int portNumber,const std::string &pAddr) = 0;
    virtual void sendMes(const std::string &mes) = 0;
    virtual bool isRunning() = 0;
    virtual void stop() = 0;
    virtual ~AClient() = 0;
};

#endif // ACLIENT_H
