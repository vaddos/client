#ifndef ADIALOGWITHUSER_H
#define ADIALOGWITHUSER_H

class ADialogWithUser
{
public:
    virtual ~ADialogWithUser() = 0;
    virtual int start() = 0;
    virtual void stop() = 0;
};

#endif // ADIALOGWITHUSER_H
