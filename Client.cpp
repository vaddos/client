#include "Client.h"
#include <string.h>
#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

Client::Client():
    running(false),
    clientSockDesc(0)
{
}


bool Client::openSocket()
{
    clientSockDesc = socket(AF_INET, SOCK_STREAM, 0);                                  // Create socket of domain - Internet (IP) address, type - Stream based (TCP) and protocol unspecified
                                                                                       // since it is only useful when underlying stack allows more than one protocol and we are choosing one.
                                                                                       // 0 means choose the default protocol.
    if(clientSockDesc < 0)                                                             // A valid descriptor is always a positive value
    {
        std::cerr <<("Failed creating socket\n");
        return false;
    }
    fcntl(clientSockDesc, F_SETFL, O_NONBLOCK);                                          //Эта операция превращает сокет в неблокирующий. Вызов любой функции с таким сокетом будет возвращать управление немедленно.
    return true;
}


bool Client::connectToServ(unsigned int portNumber,const std::string &ipAddr)
{
    assert(clientSockDesc > 0);
    if(portNumber > 65536)
    {
        std::cerr <<"ERROR, port number is too big\n";
        return false;
    }
    struct sockaddr_in serv_addr;
    if (inet_aton(ipAddr.c_str(), &serv_addr.sin_addr) == 0)
    {
         std::cerr <<("Invalid IP address\n");
         return false;
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portNumber);
    while (connect(clientSockDesc,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
    {
        if(errno == EINPROGRESS)
            continue;
        std::cerr <<"ERROR connect to server\n";
        return false;
    }
    std::cout <<"connect to server!\n";
    running = true;
    thrClient = std::thread(&Client::run,this);
    return true;
}


bool Client::sendMes()
{
    outMutex.lock();
    if(outQueque.empty())
    {
        outMutex.unlock();
        return true;
    }
    std::string mes(outQueque.front());
    outQueque.pop();
    outMutex.unlock();
    send(clientSockDesc, mes.c_str(), mes.length(), 0);
    return true;
}

bool Client::recieve()
{
    const int bufSize = 1000;
    char buffer[bufSize];
    int mesSize = recv(clientSockDesc, buffer, bufSize, 0);
    if (mesSize != 0)
    {
        if (mesSize == -1 )
        {
            if(errno == EWOULDBLOCK)                                 //EAGAIN or EWOULDBLOCK The socket is marked nonblocking and the receive operation
                return true;                                         //would block, or a receive timeout had been set and the timeout expired before data was received
            std::cerr << "Client error "<< errno;
            return false;
        }
        else
        {
            std::string strInc(buffer,mesSize);
            std::cout<< strInc<<std::endl;
        }
    }
    return true;
}

void Client::sendMes(const std::string &mes)
{
    std::lock_guard<std::mutex> lock(outMutex);
    outQueque.push(mes);
}


bool Client::isRunning()
{
    return running;
}

void Client::run()
{    
    while(running)
    {
        if(!recieve())
            break;
        if(!sendMes())
            break;
    }
    running = false;
}

bool Client::isSocketOpen()
{
    return clientSockDesc > 0;
}

void Client::stop()
{
    running = false;
    if(clientSockDesc)
    {
        close(clientSockDesc);
        clientSockDesc = 0;
    } 
}


Client::~Client()
{
    stop();
    thrClient.join();
}
