#ifndef DIALOGWITHUSER_H
#define DIALOGWITHUSER_H
#include <iostream>
#include <memory>
#include "AClient.h"
#include "ADialogWithUser.h"

class DialogWithUser : public ADialogWithUser
{
    std::string ipAddres;
    int numPort;
    std::shared_ptr<AClient> client;
public:
    DialogWithUser();
    virtual int start();
    virtual void stop();
    ~DialogWithUser();
protected:
    virtual bool checkForCorrectIpHead(const std::string &str);
private:
    DialogWithUser(const DialogWithUser&);
    DialogWithUser & operator = (const DialogWithUser &s);
    bool tryToConnectToServ();
    int chatWithUser();
};

#endif // DIALOGWITHUSER_H
