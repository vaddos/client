#include "DialogWithUser.h"
#include "Client.h"
#include <iostream>
#include <arpa/inet.h>

DialogWithUser::DialogWithUser():
    client(new Client())
{
    std::cout<<"Welcome to chat!\n";
}

bool DialogWithUser::tryToConnectToServ()
{
    bool success(false);
    do
    {
        std::cout<<"To connect to the server enter parameters.\n";
        std::cout<<"IpAddres - ";
        std::string ipAddr;
        std::cin >> ipAddr;
        std::cout<<"Port number - ";
        std::string t_StrPortNumber;
        std::cin >> t_StrPortNumber;
        int portNumber = atoi(t_StrPortNumber.c_str());
        success = client->connectToServ(portNumber,ipAddr);
        if(!success)
        {
            std::string answer;
            do
            {
                std::cout<<"Try one more time. Y/N - ";
                std::cin >> answer;
                if(answer.length() == 1)
                {
                    answer[0] = tolower(answer[0]);
                    if(answer.compare("n") == 0)
                        return false;
                }
                else
                   std::cout<<"Error read.Enter only Y or N !\n";
            }
            while (answer.compare("y") != 0);
        }
    }
    while(!success);
    return success;
}


void outHelp()
{
    std::cout<<"To user need send message in format <ip address>_<mail>.\n";
    std::cout<<"(example - \"192.168.0.1 Blah-blah-blah\")\n";
    std::cout<<"To exit from program enter - \"exit\"\n";
}

bool DialogWithUser::checkForCorrectIpHead(const std::string &str)
{
    in_addr inp;
    const unsigned int maxPosOfBackspaceInStr = 15;
    std::size_t pos = str.find(" ");
    if (pos == std::string::npos || pos > maxPosOfBackspaceInStr)
        return false;
    std::string ipAddr(str.substr(0,pos));
    inp.s_addr = 0;
    return (inet_aton(ipAddr.c_str(),&inp) != 0);
}

int DialogWithUser::chatWithUser()
{
    outHelp();
    bool exit(false);
    std::string message;
    do
    {
        std::getline (std::cin,message);
        if(message.length() > 0)
        {
            if(message.compare("help") == 0)
                outHelp();
            else if(message.compare("exit") == 0)
            {
                stop();
                break;
            }
            else
            {
                if(checkForCorrectIpHead(message))
                    client->sendMes(message);
                else
                    std::cout<< "Wrong format!\n";
            }
        }
        if(!client->isRunning())                                                         //check state client
        {
             std::cout<<"client is shutdown!\n";
             return 3;
        }
    }
    while(!exit);
    return 0;

}

DialogWithUser::~DialogWithUser()
{
    stop();
}

void DialogWithUser::stop()
{
    client->stop();
}

int DialogWithUser::start()
{
    if(!client->openSocket())
        return 1;
    if(!tryToConnectToServ())
        return 2;
    return chatWithUser();
}
