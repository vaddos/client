
QMAKE_CXXFLAGS += -std=c++0x

SOURCES += \
    main.cpp \
    Client.cpp \
    DialogWithUser.cpp \
    AClient.cpp \
    ADialogWithUser.cpp

HEADERS += \
    Client.h \
    DialogWithUser.h \
    AClient.h \
    ADialogWithUser.h

CONFIG   += console
