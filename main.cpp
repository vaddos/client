#include "DialogWithUser.h"
#include "ADialogWithUser.h"
#include <memory>


int main()
{
    std::shared_ptr<ADialogWithUser > client(new DialogWithUser());
    int ret = client->start();
    client->stop();
    return ret;
}


